﻿
namespace WebApi.Domain.Entities
{
    public class Student: BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; } 

        public DateTime Birthday { get; set; }

        public Group? Group { get; set; }
    }
}
