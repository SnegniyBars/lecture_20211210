﻿namespace WebApi.Domain.Entities
{
    public class Group : BaseEntity
    {
        public string GroupId { get; set; }
        public string Name { get; set; }
    }
}
