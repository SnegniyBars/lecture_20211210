﻿using WebApi.Domain.Entities;

namespace WebApi.Domain.Repositories
{
    public interface IStudentsRepository : IBaseRepository<Student>
    {
        Task<IEnumerable<Student>> GetByFullName(string firstName, string lastName);
    }
}
