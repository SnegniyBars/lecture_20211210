﻿using WebApi.Domain.Entities;

namespace WebApi.Domain.Repositories
{
    public interface IGroupsRepository : IBaseRepository<Group>
    {
    }
}
