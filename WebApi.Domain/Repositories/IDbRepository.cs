﻿using WebApi.Domain.Entities;

namespace WebApi.Domain.Repositories
{
    public interface IDbRepository
    {
        IGroupsRepository Groups { get; }
        IStudentsRepository Students { get; }
    }
}
