﻿namespace WebApi.Domain.Repositories
{
    public interface IBaseRepository<TEntity>
    {
        Task<TEntity[]> GetAll();
        Task<TEntity?> GetById(Guid id);
        Task Create(TEntity entity);
        Task Update(TEntity entityToUpdate);
        Task Remove(Guid id);
    }
}
