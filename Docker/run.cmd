@ECHO OFF

FOR /f "tokens=*" %%i IN ('docker ps -qf ancestor^=lecture/postgre') DO (
docker container stop %%i
docker rm %%i
)

docker image rm -f lecture/postgre
docker system prune -f
docker build -t lecture/postgre .

docker run -p 10.254.0.113:5432:5432/tcp lecture/postgre