using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Entities;
using WebApi.Domain.Repositories;
using ILogger = Serilog.ILogger;

namespace WebAppAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SampleStudentsController : ControllerBase
    {
        private ILogger _logger;
        private IDbRepository _repository;

        public SampleStudentsController(ILogger logger, IDbRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet("all")]
        public async Task<IEnumerable<Student>> GetAllStudents()
        {
            return await _repository.Students.GetAll();
        }

        [HttpGet("search")]
        public async Task<IEnumerable<Student>> GetStudentsByName(string firstName, string lastName)
        {
            return await _repository.Students.GetByFullName(firstName, lastName);
        }

        //[HttpGet(Name = "GetWeatherForecast")]
        //public IEnumerable<WeatherForecast> Get()
        //{
        //    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    {
        //        Date = DateTime.Now.AddDays(index),
        //        TemperatureC = Random.Shared.Next(-20, 55),
        //        Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        //    })
        //    .ToArray();
        //}
    }
}