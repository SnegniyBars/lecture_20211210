using Microsoft.EntityFrameworkCore;
using Serilog;
using WebApi.Domain.Repositories;
using WebApp.Data;
using WebApp.Data.Repositories;
using WebAppAPI.Services;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateBootstrapLogger();

Log.Information("Starting up");

try
{
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.UseSerilog((ctx, lc) => lc.ReadFrom.Configuration(ctx.Configuration));

    // Add services to the container.

    builder.Services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    builder.Services.AddDbContext<StudentsProjectDataContext>(o =>
    {
        o.UseNpgsql(builder.Configuration.GetConnectionString("Lecture_Students"), x =>
        {
            //x.MigrationsHistoryTable("migration_history", "dev");
        });

        if (builder.Environment.IsDevelopment()) 
        {
            o.EnableSensitiveDataLogging();
        }
    });
    
    builder.Services.AddScoped<IDbRepository, DbRepository>();
    builder.Services.AddHostedService<MigratorHostedService>();

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();

}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled exception");
}
finally
{
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}