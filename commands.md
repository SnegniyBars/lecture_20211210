# db migratoins

###	Create Initial migration

`dotnet ef migrations add Init --no-build --context "WebApp.Data.StudentsProjectDataContext" --project "..\WebApp.Data"`
`dotnet ef migrations add AddData --no-build --context "WebApp.Data.StudentsProjectDataContext" --project "..\WebApp.Data"`

`Update-Database -ProjectName "WebAppAPI"`

###	all commands see https://docs.microsoft.com/en-us/ef/core/cli/dotnet
`dotnet tool update --global dotnet-ef`

#	Add packets

###	add serilog
`dotnet add package Serilog.AspNetCore`



#	GRPC

- Grpc.Net.Client, который содержит клиент .NET Core.
- Google.Protobuf, который содержит API сообщений protobuf для C#;
- Grpc.Tools, который содержит поддержку инструментов C# для файлов protobuf. Пакет инструментов не требуется во время выполнения, поэтому зависимость помечается PrivateAssets="All".

