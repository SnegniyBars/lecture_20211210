﻿using Microsoft.EntityFrameworkCore;
using WebApi.Domain.Entities;

namespace WebApp.Data
{
    public class StudentsProjectDataContext : DbContext
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        //public StudentsProjectDataContext() { }
        public StudentsProjectDataContext(DbContextOptions options) : base(options) { }

        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSnakeCaseNamingConvention();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var group = new Group { Id = Guid.NewGuid(), GroupId = "10-is-22", Name = "is" };
            modelBuilder.Entity<Group>().HasData(group);

            modelBuilder.Entity<Student>().HasData(
                    new Student { Id = Guid.NewGuid(), FirstName = "Tom", LastName="test",  Birthday = new DateTime(2000, 01, 20), },
                    new Student { Id = Guid.NewGuid(), FirstName = "Alice", LastName = "test", Birthday = new DateTime(2000, 07, 28)},
                    new Student { Id = Guid.NewGuid(), FirstName = "Sam", LastName = "test", Birthday = new DateTime(1999, 12, 20), }
            );
        }
    }
}