﻿using Microsoft.EntityFrameworkCore;
using WebApi.Domain;
using WebApi.Domain.Repositories;

namespace WebApp.Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IEntity
    {
        private StudentsProjectDataContext _dbContext;
        protected DbSet<TEntity> _dbSet;

        public BaseRepository(StudentsProjectDataContext dbContext)
        {            
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public async Task Create(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task<TEntity[]> GetAll()
        {            
            return await _dbSet.ToArrayAsync();
        }

        public async Task<TEntity?> GetById(Guid id)
        {            
            return await _dbSet.FindAsync(id);
        }

        public Task Remove(Guid id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            _dbSet.Remove(entityToDelete);
            return Task.CompletedTask;
        }

        public Task Update(TEntity entityToUpdate)
        {
            _dbSet.Update(entityToUpdate);
            return Task.CompletedTask;
        }
    }
}
