﻿using Microsoft.EntityFrameworkCore;
using WebApi.Domain.Entities;
using WebApi.Domain.Repositories;

namespace WebApp.Data.Repositories
{
    public class StudentsRepository : BaseRepository<Student>, IStudentsRepository
    {
        public StudentsRepository(StudentsProjectDataContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<Student>> GetByFullName(string firstName, string lastName)
        {
            var query = _dbSet.AsQueryable();
            if (firstName != null && firstName != "")
            {
                query = query.Where(x => EF.Functions.ILike(x.FirstName, $"{firstName}%"));
            }
            if (lastName != null && lastName != "")
            {
                query = query.Where(x => EF.Functions.ILike(x.LastName, $"{lastName}%"));
            }
            return await query.ToArrayAsync();
        }
    }
}
