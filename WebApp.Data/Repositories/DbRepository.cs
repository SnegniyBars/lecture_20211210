﻿using WebApi.Domain.Entities;
using WebApi.Domain.Repositories;

namespace WebApp.Data.Repositories
{
    public class DbRepository: IDbRepository
    {
        private StudentsProjectDataContext _dbContext;

        public DbRepository(StudentsProjectDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IGroupsRepository Groups => new GroupsRepository(_dbContext);

        public IStudentsRepository Students => new StudentsRepository(_dbContext);


        //public async Task<int> SaveAsync()
        //{
        //    return await context.SaveChangesAsync();
        //}

    }
}
