﻿using WebApi.Domain.Entities;
using WebApi.Domain.Repositories;

namespace WebApp.Data.Repositories
{
    public class GroupsRepository : BaseRepository<Group>, IGroupsRepository
    {
        public GroupsRepository(StudentsProjectDataContext dbContext) : base(dbContext)
        {
        }
    }
}
