﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApp.Data.Migrations
{
    public partial class AddData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_students_groups_group_id",
                table: "students");

            migrationBuilder.DropColumn(
                name: "created",
                table: "students");

            migrationBuilder.DropColumn(
                name: "created",
                table: "groups");

            migrationBuilder.AlterColumn<Guid>(
                name: "group_id",
                table: "students",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.InsertData(
                table: "groups",
                columns: new[] { "id", "group_id", "name" },
                values: new object[] { new Guid("bc4090ed-96fa-4019-a645-7907eabea73e"), "10-is-22", "is" });

            migrationBuilder.InsertData(
                table: "students",
                columns: new[] { "id", "birthday", "first_name", "group_id", "last_name" },
                values: new object[,]
                {
                    { new Guid("38210595-8a1b-4dbc-9bfb-b8ff3c46492d"), new DateTime(2000, 1, 20, 0, 0, 0, 0, DateTimeKind.Utc), "Tom", null, "test" },
                    { new Guid("549a2f31-cacd-4a0d-89ac-3d3bccf75486"), new DateTime(1999, 12, 20, 0, 0, 0, 0, DateTimeKind.Utc), "Sam", null, "test" },
                    { new Guid("ee7223f7-97de-45c4-97ad-eedb1209a70b"), new DateTime(2000, 7, 28, 0, 0, 0, 0, DateTimeKind.Utc), "Alice", null, "test" }
                });

            migrationBuilder.AddForeignKey(
                name: "fk_students_groups_group_id",
                table: "students",
                column: "group_id",
                principalTable: "groups",
                principalColumn: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_students_groups_group_id",
                table: "students");

            migrationBuilder.DeleteData(
                table: "groups",
                keyColumn: "id",
                keyValue: new Guid("bc4090ed-96fa-4019-a645-7907eabea73e"));

            migrationBuilder.DeleteData(
                table: "students",
                keyColumn: "id",
                keyValue: new Guid("38210595-8a1b-4dbc-9bfb-b8ff3c46492d"));

            migrationBuilder.DeleteData(
                table: "students",
                keyColumn: "id",
                keyValue: new Guid("549a2f31-cacd-4a0d-89ac-3d3bccf75486"));

            migrationBuilder.DeleteData(
                table: "students",
                keyColumn: "id",
                keyValue: new Guid("ee7223f7-97de-45c4-97ad-eedb1209a70b"));

            migrationBuilder.AlterColumn<Guid>(
                name: "group_id",
                table: "students",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "created",
                table: "students",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

            migrationBuilder.AddColumn<DateTime>(
                name: "created",
                table: "groups",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

            migrationBuilder.AddForeignKey(
                name: "fk_students_groups_group_id",
                table: "students",
                column: "group_id",
                principalTable: "groups",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
